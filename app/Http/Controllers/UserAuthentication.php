<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
class UserAuthentication extends Controller {

    // This function will login the user and return user data with HASH API Tokens
    function userLogin(Request $request) {
        // Validation user data
        $validator = Validator::make($request->all(), [
            "email" => "required|string|email|max:255",
            "password" => "required|string|max:255"
        ]);

        if ($validator->fails()) {
            return response([
                'status' => false,
                'code' => 422,
                'message' => 'Validation Failed. Please check input email and password',
                'data' => [
                    'user' => null,
                    'token' => null
                ]
                    ], 422);
        }

        // Get user information from database
        $user = User::where('email', $request->email)->first();
        // Match with password
        if (!$user || !Hash::check($request->password, $user->password)) {
            // User not match
            return response([
                'status' => false,
                'code' => 403,
                'message' => 'These credentials doesnt match.',
                'data' => [
                    'user' => null,
                    'token' => null
                ]
                    ], 403);
        } else {
            // User match
            $token = $user->createToken('api-login')->plainTextToken;
            $response = [
                'status' => true,
                'code' => 200,
                'message' => 'These credentials match.',
                'data' => [
                    'user' => $user,
                    'token' => $token,
                ]
            ];
            return response($response, 201);
        }
    }

    // This function will logout user
    function userLogout() {
        $user = request()->user();
        $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
    }

}
