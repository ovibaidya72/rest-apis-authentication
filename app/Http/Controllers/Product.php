<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductModel;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class Product extends Controller {
    /*
      |--------------------------------------------------------------------------
      |                                   For WEB
      |--------------------------------------------------------------------------
      |
     */


    /*
      |--------------------------------------------------------------------------
      |                                   For API
      |--------------------------------------------------------------------------
      |
     */

    // This controller will create a new product with api
    function createNewProduct(Request $request) {
        // Validation input data
        $validator = Validator::make($request->all(), [
                    "product_title" => "required|string|max:255",
                    "description" => "required|string|max:3000",
                    "price" => "required",
        ]);

        if ($validator->fails()) {
            return response([
                'status' => false,
                'code' => 422,
                'message' => 'Validation Failed. Please check input data.',
                'data' => []
                    ], 422);
        }
        // Save to database
        $createNewProduct = ProductModel::create([
                    "name" => $request->product_title,
                    "slug" => Str::slug($request->product_title) . "-" . uniqid(),
                    "price" => $request->price,
                    "description" => $request->description
        ]);
        if ($createNewProduct) {
            // Successfully save new product
            return response([
                'status' => true,
                'code' => 200,
                'message' => 'Product save successfully',
                'data' => $createNewProduct
                    ], 200);
        } else {
            // Product not save
            return response([
                'status' => false,
                'code' => 422,
                'message' => 'Product save failed',
                'data' => []
                    ], 422);
        }
    }

    // This function will return products details
    function getAllProducts() {
        // Return all product with all values
        $productDetails = ProductModel::all();
        return response([
            'status' => true,
            'code' => 200,
            'message' => 'All Product List',
            'data' => $productDetails
                ], 200);
    }

    // This function will update existing product by product ID
    function updateProductById(Request $request) {
        // Validation input data
        $validator = Validator::make($request->all(), [
                    "product_id" => "required|integer",
                    "product_title" => "string|max:255",
                    "description" => "string|max:3000",
        ]);

        if ($validator->fails()) {
            return response([
                'status' => false,
                'code' => 422,
                'message' => 'Validation Failed. Please check input data.',
                'data' => []
                    ], 422);
        }
        // Get Product Previous Details
        $productPreviousDetails = ProductModel::where("id", $request->product_id)->first();
        if (empty($productPreviousDetails)) {
            // Product Not Found
            return response([
                'status' => false,
                'code' => 404,
                'message' => 'Product not found!!',
                'data' => []
                    ], 404);
        }
        // update to database
        $updateProductDetails = ProductModel::where("id", $request->product_id)->update([
            "name" => (isset($request->product_title) ? $request->product_title : $productPreviousDetails->name), // If product_title not given then it will use previous one 
            "price" => (isset($request->price) ? $request->price : $productPreviousDetails->price), // If price not given then it will use previous one 
            "description" => (isset($request->description) ? $request->description : $productPreviousDetails->description) // If description not given then it will use previous one 
        ]);
        return response([
            'status' => ($updateProductDetails) ? true : false,
            'code' => 200,
            'message' => ($updateProductDetails) ? 'Product Update Successfull' : 'Product Update Fail',
            'data' => []
                ], 200);
    }

    // This function will hard delete a product from database
    function deleteProductById(Request $request) {
        // Validation input data
        $validator = Validator::make($request->all(), [
                    "product_id" => "required|integer",
        ]);

        if ($validator->fails()) {
            return response([
                'status' => false,
                'code' => 422,
                'message' => 'Validation Failed. Please check input data.',
                'data' => []
                    ], 422);
        }
        // Hard Delete Product from database
        $deleteProduct = ProductModel::where("id", $request->product_id)->delete();
        return response([
            'status' => ($deleteProduct) ? true : false,
            'code' => 200,
            'message' => ($deleteProduct) ? 'Product Delete Success' : 'Product Delete Failed',
            'data' => []
                ], 200);
    }

    // This function will search product by name
    function searchProduct(Request $request) {
        // Validation input data
        $validator = Validator::make($request->all(), [
                    "product_title" => "required|string",
        ]);

        if ($validator->fails()) {
            return response([
                'status' => false,
                'code' => 422,
                'message' => 'Validation Failed. Please check input data.',
                'data' => []
                    ], 422);
        }
        // Find Result from Database
        $searchResult = ProductModel::where('name', 'like', "%$request->product_title%")->get();
        return response([
            'status' => true,
            'code' => 200,
            'message' => 'Here is your search result.',
            'data' => $searchResult
                ], 200);
    }

}
