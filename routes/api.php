<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
// Controllers
use App\Http\Controllers\UserAuthentication; // Use for user authentication
use App\Http\Controllers\Product; // Use for product controller


/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

// This route group could be access if user is admin only 
Route::group(['middleware' => 'auth:sanctum'], function () {
    // Admin Authorized Routes
    // Create a new product
    Route::post("/admin/create-new-product",[Product::class,"createNewProduct"]);
    // Update Product
    Route::post("/admin/product-update",[Product::class,"updateProductById"]);
    // Get all Products
    Route::get("/admin/get-all-product",[Product::class,"getAllProducts"]);
    // Delete Product
    Route::post("/admin/product-delete",[Product::class,"deleteProductById"]);
    // Search Product
    Route::get("/admin/product-search",[Product::class,"searchProduct"]);
    Route::get("/admin/logout",[UserAuthentication::class,"userLogout"]);
});
// Login Route
Route::post("/admin/login", [UserAuthentication::class, "userLogin"]);
