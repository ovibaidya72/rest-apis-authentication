<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
class ProductSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // Seed demo products
        DB::table('products')->insert([
            [
                "name" => "Product Name 1",
                "slug" => Str::slug("Product Name 1"),
                "price" => 100,
                "description" => "This is a demo product 1"
            ],
            [
                "name" => "Product Name 2",
                "slug" => Str::slug("Product Name 2"),
                "price" => 200,
                "description" => "This is a demo product 2"
            ],
            [
                "name" => "Product Name 3",
                "slug" => Str::slug("Product Name 3"),
                "price" => 300,
                "description" => "This is a demo product 3"
            ],
            [
                "name" => "Product Name 4",
                "slug" => Str::slug("Product Name 4"),
                "price" => 400,
                "description" => "This is a demo product 4"
            ]
        ]);
    }

}
