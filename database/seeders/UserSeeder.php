<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed a Test User for Database
        DB::table('users')->insert([
            'name' => "Debdulal Baidya",
            'email' => "ovibaidya72@gmail.com",
            'password' => Hash::make("bangladesh"),
            'user_role' => "admin"
        ]);
    }
}
