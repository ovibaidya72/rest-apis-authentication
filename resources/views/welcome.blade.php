<html>
    <head>
        <title>Assessment</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Source+Serif+Pro&display=swap" rel="stylesheet">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body style="font-family: 'Source Serif Pro', serif;">
        <div class="container">
            
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center">User Menual</h1><hr>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <h3><u>Make it Ready</u></h3>
                    <h5>Download: git clone https://gitlab.com/ovibaidya72/rest-apis-authentication.git</h5>
                    <h5>Step 1: <br>Create database and input all the information to .env file.</h5>
                    <h5>Step 2:<br> Migrate and seed the database. <br>Command: php artisan migrate:fresh --seed </h5>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <h3><u>Login/Authentication</u></h3>
                    
                    <h4><b>API Route: </b>/api/admin/login</h4>
                    <h4><b>Route Type: </b>Public</h4>
                    <h4><b>Request Type: </b>Post</h4>
                    <h4>Request body:</h4>
                    <ul>
                        <li>email &nbsp;&nbsp;(Required,string,max:255)</li>
                        <li>password &nbsp;&nbsp;(Required,string)</li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <p><b>Note: </b>After migrate and seed the database, a user will be create with <br><b>email:</b> ovibaidya72@gmail.com<br><b>password: </b> bangladesh <br><br></p>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <h3><u>Product Create</u></h3>
                    <h4><b>API Route: </b>/api/admin/create-new-product</h4>
                    <h4><b>Route Type: </b>Protected - Admin can access</h4>
                    <h4><b>Request Type: </b>Post</h4>
                    <h4>Request body:</h4>
                    <ul>
                        <li>product_title &nbsp;&nbsp;(Required,string,max:255)</li>
                        <li>price &nbsp;&nbsp;(Required,double)</li>
                        <li>description &nbsp;&nbsp;(Required,string,max:3000)</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3><u>Product List</u></h3>
                    <h4><b>API Route: </b>/admin/get-all-product</h4>
                    <h4><b>Route Type: </b>Protected - Admin can access</h4>
                    <h4><b>Request Type: </b>Get</h4>
                    <h4>Request body:</h4>
                    <ul>
                        <li>--</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3><u>Product Update</u></h3>
                    <h4><b>API Route: </b>/admin/product-update</h4>
                    <h4><b>Route Type: </b>Protected - Admin can access</h4>
                    <h4><b>Request Type: </b>Post</h4>
                    <h4>Request body:</h4>
                    <ul>
                        <li>product_id &nbsp;&nbsp;(Required,int)</li>
                        <li>product_title &nbsp;&nbsp;(string,max:255)</li>
                        <li>price &nbsp;&nbsp;(double)</li>
                        <li>description &nbsp;&nbsp;(string,max:3000)</li>
                    </ul>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <h3><u>Product Delete</u></h3>
                    <h4><b>API Route: </b>/admin/product-delete</h4>
                    <h4><b>Route Type: </b>Protected - Admin can access</h4>
                    <h4><b>Request Type: </b>Post</h4>
                    <h4>Request body:</h4>
                    <ul>
                        <li>product_id &nbsp;&nbsp;(Required,int)</li>
                    </ul>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <h3><u>Product Search</u></h3>
                    <h4><b>API Route: </b>/admin/product-search</h4>
                    <h4><b>Route Type: </b>Protected - Admin can access</h4>
                    <h4><b>Request Type: </b>Get</h4>
                    <h4>Request body:</h4>
                    <ul>
                        <li>product_title &nbsp;&nbsp;(Required,string)</li>
                    </ul>
                </div>
            </div>
             <div class="row">
                <div class="col-md-12">
                    <h3><u>Logout</u></h3>
                    <h4><b>API Route: </b>/admin/logout</h4>
                    <h4><b>Route Type: </b>Protected - Admin can access</h4>
                    <h4><b>Request Type: </b>Get</h4>
                    <h4>Request body:</h4>
                    <ul>
                        <li>--</li>
                    </ul>
                </div>
            </div>
            <div class="row" style="height: 200px;">
                
            </div>
        </div>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
